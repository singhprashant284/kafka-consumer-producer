package com.kpc.consumer;

import com.kpc.entity.User;
import com.kpc.utils.AppConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafKaConsumer {
    @KafkaListener(topics = AppConstants.TOPIC_NAME,
            groupId = AppConstants.GROUP_ID)
    public void consume(User data){
        log.info(String.format("Message received -> %s", data.toString()));
    }
}
